FROM golang:1.13.6 as collector_builder


# get trainmvp dependencies
# RUN go get -d -v github.com/prometheus/client_golang/prometheus && \
#     go get -d -v github.com/prometheus/client_golang/prometheus/promhttp &&\
#     go get -d -v github.com/nats-io/nats.go

# get train mvp  ( this repo )
#RUN go get -d -v  bitbucket.org/cocoon/trainmvp
#RUN go bitbucket.org/cocoon/trainmvp/cmd/collector
RUN git clone https://bitbucket.org/cocoon/web-collector.git
WORKDIR /go/web-collector
## Add this go mod download command to pull in any dependencies
RUN go mod download

## build collector
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o collector .
#CMD ["bash"]

FROM alpine:latest

RUN sed -i.bak 's+https://+http://+' /etc/apk/repositories
RUN apk --no-cache add ca-certificates


WORKDIR /app/
RUN chmod +w /app
COPY --from=collector_builder /go/web-collector/collector .
COPY --from=collector_builder /go/web-collector/assets assets/

EXPOSE 8080

#CMD ["./collector","--nats","nats://127.0.0.1:4222","--host",":8080"]
CMD ["ash"]

