package cook

import (
	"bitbucket.org/cocoon/web-collector/app/mock"
	"bitbucket.org/cocoon/web-collector/assets"
	"os"
	"testing"
)

func TestDocumentTemplater(t *testing.T) {

	api, _ := mock.NewCollectorRequesterApi()

	data, err := DocumentListTemplater(api)
	if err != nil {
		t.Fail()
		return
	}
	_ = data

	a := assets.NewAssets("", "")
	a.LoadTemplate()

	a.RenderScreen(os.Stdout, "watcher_documentation.tmpl", data)

}
