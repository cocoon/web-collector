package assets

import (
	"log"
	"os"
	"testing"
)

type Entry struct {
}
type Entries []*Entry

func TestAssets(t *testing.T) {

	a := NewAssets("../../assets", "demo")
	err := a.LoadTemplate()
	if err != nil {
		log.Printf(err.Error())
		t.Fail()
		return
	}

	name := "home.tmpl"
	data := Entries{}
	_ = data

	// test home page
	a.RenderScreen(os.Stdout, name, map[string]interface{}{
		"Version": "0.1",
	})

	println("Done")

}
