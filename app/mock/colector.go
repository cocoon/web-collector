package mock

import (
	api "bitbucket.org/cocoon/trainmvp/api"
	origin "bitbucket.org/cocoon/trainmvp/collector"
	//"bitbucket.org/cocoon/trainmvp/documentor"
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
)

/*

	mock the collector requester api

	implements trainmvp/api CollCollectorRequester

	type CollectorRequester interface {
		GetWatcherDocumentation() (documentor.WatcherDocumentation, error)
		GetWatcherStatus() (map[string]origin.TimeSample, error)
	}
}


*/

// CollectorRequesterApi implements meeting/pkg/api MeetingApi
type CollectorRequesterApi struct {
	root string
}

func (a CollectorRequesterApi) path(filename string) string {
	return filepath.Join(a.root, filename)
}

func NewCollectorRequesterApi() (CollectorRequesterApi, error) {

	api := CollectorRequesterApi{}

	self := getSelfPath()
	root := filepath.Join(self, "files")
	api.root = root
	_, err := os.Stat(root)
	return api, err
}

func (a CollectorRequesterApi) GetWatcherDocumentation() (d api.WatcherDocumentation, err error) {

	//var list []*event.EventListResponse
	//
	// take raw response from  ../demo/files/response_events.txt
	content, err := ioutil.ReadFile(a.path("documentation.json"))
	if err != nil {
		return
	}
	err = json.Unmarshal(content, &d)

	return
}

func (a CollectorRequesterApi) GetWatcherStatus() (s map[string]origin.TimeSample, err error) {

	content, err := ioutil.ReadFile(a.path("watcherstatus.json"))
	if err != nil {
		return
	}
	err = json.Unmarshal(content, &s)
	return
}

// utils

func getSelfPath() string {
	// return the path of this file ( assets.go )
	_, filename, _, ok := runtime.Caller(1)
	if ok == true {
		return filepath.Dir(filename)
	}
	return ""
}
