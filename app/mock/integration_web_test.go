// build integration

package mock

/*

	start a nats server
	start a collector

	inject sample watcher doc
	inject sample watcher status

	start a web-collector


	run it :

		go test -tags=integration

*/
import (
	"bitbucket.org/cocoon/trainmvp/watcher"
	"context"
	"encoding/json"
	"io/ioutil"
	//"strings"
	"testing"

	"bitbucket.org/cocoon/trainmvp/api"
	origin "bitbucket.org/cocoon/trainmvp/collector"
	web "bitbucket.org/cocoon/web-collector/app/server"
	assets "bitbucket.org/cocoon/web-collector/assets"
)

//var fooAddr = flag.String(...)

var natsURL = "nats://127.0.0.1:4222"

func injectFakeDocumentation(a *api.API) (err error) {

	cli, err := a.DocumentorClient()
	if err != nil {
		return err
	}

	data, err := ioutil.ReadFile("./files/documentation.json")
	if err != nil {
		return
	}
	doc := api.WatcherDocumentation{}
	err = json.Unmarshal(data, &doc)
	if err != nil {
		return
	}
	for watcher, checks := range doc {

		for check, entry := range checks {
			err = cli.AddEntry(watcher, check, entry)
			if err != nil {
				return
			}
		}
	}
	return err
}
func injectFakeWatcherStatus(a *api.API) (err error) {

	cli, err := a.CollectorClient()
	if err != nil {
		return err
	}
	data, err := ioutil.ReadFile("./files/watcherstatus.json")
	if err != nil {
		return
	}
	var stats map[string]origin.TimeSample
	err = json.Unmarshal(data, &stats)
	if err != nil {
		return
	}
	for subject, entry := range stats {
		sub := &watcher.WatcherSubject{}
		err := sub.Parse(subject)
		if err == nil {
			err = cli.PublishWatcherStatus2(sub, entry.Status)
			_ = cli
			_ = entry
		}

		//parts := strings.Split(subject, ".")
		//if len(parts) < 3 {
		//	continue
		//}
		//prefix := parts[0]
		//watcher := parts[1]
		//check := parts[2]
		//if prefix == "watcher" {
		//	err = cli.PublishWatcherStatus(watcher, check, entry.Status)
		//}
	}

	all, err := a.GetWatcherStatus()
	if err != nil {
		return
	}
	_ = all

	// err = cli.PublishWatcherStatus("watcher1", "check1", 1)
	// err = cli.PublishWatcherStatus("watcher1", "check2", -1)
	// err = cli.PublishWatcherStatus("watcher2", "check1", 0)
	return
}

func TestWebCollector(t *testing.T) {

	// f, err := foo.Connect(*fooAddr)
	// ...
	ctx, cancel := context.WithCancel(context.Background())

	// assume nats-server running

	// create a collector ans start it
	c := api.NewAPI(ctx, natsURL)
	err := c.Start()
	if err != nil {
		t.Fail()
		return
	}

	err = injectFakeDocumentation(c)
	if err != nil {
		t.Fail()
		return
	}

	err = injectFakeWatcherStatus(c)
	if err != nil {
		t.Fail()
		return
	}

	// create web server
	a := assets.NewAssets("", "")
	err = a.LoadTemplate()
	if err != nil {
		t.Fail()
		return
	}

	s := web.NewCollectorServer(ctx, a, c)

	s.Start(":8080")

	_ = cancel

}
