package collector

import (
	"bitbucket.org/cocoon/trainmvp/documentor"
	"context"
	"fmt"
	"testing"
	"time"
)

func TestNewCollector(t *testing.T) {

	ctx := context.Context(context.Background())
	urlNats := "nats://127.0.0.1:4222"
	api := NewCollector(ctx, urlNats)

	// start collector services
	api.Start()
	time.Sleep(1 * time.Second)

	// a client to emit watcher status
	msgEmitter, err := api.CollectorClient()
	if err != nil {
		t.Fail()
		return
	}
	msgEmitter.PublishWatcherStatus("watcher1", "check1", 1)

	docEmitter, err := api.DocumentorClient()
	if err != nil {
		t.Fail()
		return
	}

	entry := documentor.Entry{
		Version: "0.1",
		Text:    "this is documentation",
	}
	docEmitter.AddEntry("watcher1", "check1", entry)
	docEmitter.AddEntry("watcher1", "check2", entry)
	docEmitter.AddEntry("watcher2", "check1", entry)

	// test receivers
	time.Sleep(1)

	doc, err := docEmitter.GetDocumentation()
	if err != nil {
		t.Fail()
		return
	}
	fmt.Printf("%v", doc)

}
