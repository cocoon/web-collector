package assets

import (
	"errors"
	"fmt"
	echo "github.com/labstack/echo/v4"
	"html/template"
	"io"
	"log"
	"net/http"
	"path"
	"path/filepath"
	"runtime"
)

/*

	templateManager

	assets.go
	static/
	tempates/layout/*.tmpl
	templates/demo/*.tmpl
	*.tmpl

	usage:
		a := NewAssets()
		err := a.LoadTemplates

 		// render template
		a.RenderTemplate(w http.ResponseWriter, name string, data interface{}) error
		or
		a.RenderScreen(w io.Writer, name string, data interface{}) error
		or

*/

var mainTmpl = `{{define "main" }} {{ template "base" . }} {{ end }}`

// implements echo renderer
type EchoRenderer struct {
	assets *Assets
}

func (s *EchoRenderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return s.assets.RenderEcho(w, name, data, c)
}

type Assets struct {
	Root        string
	StaticDir   string
	TemplateDir string

	nameSpace string
	//Templates   		*template.Template
	//MainTemplate		*template.Template

	templates map[string]*template.Template
}

func NewAssets(root string, nameSpace string) *Assets {

	if root == "" {
		// if no root specified get this directory
		here := getSelfPath()
		root = path.Dir(here)
	}

	a := Assets{
		Root:        root,
		StaticDir:   path.Join(root, "static"),
		TemplateDir: path.Join(root, "templates"),
		nameSpace:   nameSpace,
		templates:   make(map[string]*template.Template),
	}
	return &a
}

func getSelfPath() string {
	// return the path of this file ( assets.go )
	_, filename, _, ok := runtime.Caller(1)
	if ok == true {
		return filename
	}
	return ""
}

func (a *Assets) LoadTemplate() (err error) {

	if a.templates == nil {
		a.templates = make(map[string]*template.Template)
	}
	layoutDir := path.Join(a.TemplateDir, "layouts", "*.tmpl")
	layoutFiles, err := filepath.Glob(layoutDir)
	if err != nil {
		return err
	}
	if len(layoutFiles) <= 0 {
		return errors.New("No templates files found at " + layoutDir + "\n")
	}

	includeDir := path.Join(a.TemplateDir, a.nameSpace, "*.tmpl")
	includeFiles, err := filepath.Glob(includeDir)
	if err != nil {
		return err
	}
	if len(includeFiles) <= 0 {
		return errors.New("No templates files found at " + includeDir + "\n")
	}
	mainTemplate := template.New("main")
	//  `{{define "main" }} {{ template "base" . }} {{ end }}`
	mainTemplate, err = mainTemplate.Parse(mainTmpl)
	if err != nil {
		return err
	}
	for _, file := range includeFiles {
		fileName := filepath.Base(file)
		files := append(layoutFiles, file)
		a.templates[fileName], err = mainTemplate.Clone()
		if err != nil {
			return err
		}
		a.templates[fileName], err = a.templates[fileName].ParseFiles(files...)
		if err != nil {
			return err
		}
	}
	log.Println("templates loading successful")
	return nil

}

func (a *Assets) RenderHttp(w http.ResponseWriter, name string, data interface{}) error {
	tmpl, ok := a.templates[name]
	if !ok {
		http.Error(w, fmt.Sprintf("The template %s does not exist.", name),
			http.StatusInternalServerError)
		err := errors.New("Template doesn't exist")
		return err
	}
	//buf := bufpool.Get()
	//defer bufpool.Put(buf)
	err := tmpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		err := errors.New("Template execution failed")
		return err
	}
	//w.Header().Set("Content-Type", "text/html; charset=utf-8")
	//buf.WriteTo(w)
	return nil
}

func (a *Assets) RenderScreen(w io.Writer, name string, data interface{}) error {

	tmpl, ok := a.templates[name]
	if !ok {
		log.Println(fmt.Sprintf("The template %s does not exist.", name))
		err := errors.New("Template doesn't exist")
		if err != nil {
			return err
		}
	}
	//buf := bufpool.Get()
	//defer bufpool.Put(buf)
	err := tmpl.Execute(w, data)
	if err != nil {
		log.Printf("%s\n", err.Error())
		err := errors.New("Template execution failed")
		if err != nil {
			return err
		}
	}
	return nil
}

func (a *Assets) RenderEcho(w io.Writer, name string, data interface{}, c echo.Context) error {
	// Add global methods if data is a map
	if viewContext, isMap := data.(map[string]interface{}); isMap {
		viewContext["reverse"] = c.Echo().Reverse
	}

	//return t.templates.ExecuteTemplate(w, name, data)
	tmpl, ok := a.templates[name]
	if !ok {
		return echo.NewHTTPError(
			http.StatusInternalServerError,
			fmt.Sprintf("The template %s does not exist.", name))
	}
	//buf := bufpool.Get()
	//defer bufpool.Put(buf)
	err := tmpl.Execute(w, data)
	if err != nil {
		return echo.NewHTTPError(
			http.StatusInternalServerError, err.Error())
	}
	//w.Header().Set("Content-Type", "text/html; charset=utf-8")
	//buf.WriteTo(w)
	return nil
}

func (a *Assets) GetEchoRenderer() *EchoRenderer {
	s := EchoRenderer{a}
	return &s
}
