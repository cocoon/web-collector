package cook

import (
	watcher2 "bitbucket.org/cocoon/trainmvp/watcher"
	"fmt"
	"sort"
	"time"

	"bitbucket.org/cocoon/trainmvp/api"
	//origin "bitbucket.org/cocoon/trainmvp/collector"
)

type WatcherStatusTemplateData struct {
	Watcher   string
	Check     string
	Timestamp string
	Status    string // OK KO --

	Text    string
	Version string

	Table string // table-primary  or ""
	Badge string // bg-success :1 , bg-danger: -1  bg-warning =0

	//Header bool
}

func WatcherStatusTemplater(api api.CollectorRequester, filter string) (data []*WatcherStatusTemplateData, err error) {

	// filter :  all -> all watchers
	//			 alert -> only watchers with status KO and Down

	switch filter {
	case "alert":
		filter = "alert"
	default:
		filter = "all"
	}

	//filter := "NotOK" //  "problem" : only KO and down watchers   otherwise "all"
	//_ = filter

	source, err := api.GetWatcherStatus()
	if err != nil {
		return data, err
	}

	var subjectKeys []string
	// sunject like watcher.check   eg watcher1.check1
	for k, _ := range source {
		subjectKeys = append(subjectKeys, k)
	}
	sort.Strings(subjectKeys)

	lastWatcher := "first"
	var buffer []*WatcherStatusTemplateData
	for _, subject := range subjectKeys {
		sub := watcher2.WatcherSubject{}
		err := sub.Parse(subject)
		//parts := strings.Split(subject, ".")
		if err != nil {
			// bad format for subject
			continue
		}

		if sub.Prefix != "watcher" {
			continue
		}
		watcher := fmt.Sprintf("%s.%s.%s", sub.Platform, sub.Component, sub.WatcherID)
		check := sub.CheckID
		status := source[subject].Status
		badge := "bg-warning"
		statusLabel := "--"
		if status == 1 {
			badge = "bg-success"
			statusLabel = "OK"
		}
		if status == -1 {
			badge = "bg-danger"
			statusLabel = "KO"
		}
		// header := false
		// if check == "main" {
		// 	header = true
		// }

		entry := &WatcherStatusTemplateData{
			Watcher:   watcher,
			Check:     check,
			Timestamp: source[subject].Timestamp.Format(time.RFC3339),
			Status:    statusLabel,
			Badge:     badge,
			//Header: header,
		}
		if watcher != lastWatcher {
			// treat cummulated check lines
			lastWatcher = watcher
			if len(buffer) > 0 {
				lines := TreatBuffer(buffer, filter)
				data = append(data, lines...)
				buffer = nil
			}
		}
		buffer = append(buffer, entry)
	}

	// add last buffer
	if len(buffer) > 0 {
		lines := TreatBuffer(buffer, filter)
		data = append(data, lines...)
		buffer = nil
	}

	return
}

// func TreatWatcher( status string, filter string, buffer []*WatcherStatusTemplateData) (data []*WatcherStatusTemplateData){

// 	if len(buffer) > 0 {
// 		lines := TreatBuffer(buffer)
// 		data = append(data, lines...)
// 		buffer = nil
// 	}
// 	return
// }

func TreatBuffer(buffer []*WatcherStatusTemplateData, filter string) []*WatcherStatusTemplateData {

	// filter == "all"  disp

	// buffer contains all lines of a watcher
	if len(buffer) == 0 {
		return buffer
	}
	if len(buffer) == 1 {
		// only one line set primary
		buffer[0].Table = "table-primary"
		if filter != "all" {
			if buffer[0].Status == "OK" {
				// we dont want OK statuses
				return nil
			}
		}
		return buffer
	}

	// re order buffer to place check main at the top
	var newBuffer []*WatcherStatusTemplateData
	for _, line := range buffer {
		if line.Check == "main" {
			if filter != "all" {
				if line.Status == "OK" {
					// we dont want OK statuses
					return nil
				}
			}
			line.Table = "table-primary"
			newBuffer = append(newBuffer, line)
			break
		}
	}
	for _, line := range buffer {
		if line.Check != "main" {
			newBuffer = append(newBuffer, line)
		}
	}
	return newBuffer

}
