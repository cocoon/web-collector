package mock

import (
	"testing"
	"time"

	web "bitbucket.org/cocoon/web-collector/app/server"
	"bitbucket.org/cocoon/web-collector/assets"
	"github.com/labstack/gommon/log"
	"golang.org/x/net/context"
)

func TestApi(t *testing.T) {

	api, err := NewCollectorRequesterApi()
	if err != nil {
		t.Fail()
		return
	}

	watchers, err := api.GetWatcherStatus()
	if err != nil {
		t.Fail()
	}
	_ = watchers

	documentation, err := api.GetWatcherDocumentation()
	if err != nil {
		t.Fail()
	}
	_ = documentation

}

func TestWebServer(t *testing.T) {

	ctx, cancel := context.WithCancel(context.Background())
	a := assets.NewAssets("", "")
	err := a.LoadTemplate()
	if err != nil {
		log.Fatal(err.Error())
	}

	api, err := NewCollectorRequesterApi()
	if err != nil {
		log.Fatal(err.Error())
	}

	server := web.NewCollectorServer(ctx, a, api)
	_ = server

	go server.Start(":8080")

	time.Sleep(120 * time.Second)
	cancel()
	time.Sleep(2 * time.Second)
	//Server()

}
