package cook

import (
	"sort"

	"bitbucket.org/cocoon/trainmvp/api"
)

type DocumentationTemplateData struct {
	Watcher string
	Check   string
	Text    string
	Version string
	Header  bool
}

func DocumentListTemplater(api api.CollectorRequester) (documentation []*DocumentationTemplateData, err error) {

	// fetch documentation list from api
	data, err := api.GetWatcherDocumentation()
	if err != nil {
		return
	}
	// serialize dict to template data
	_ = data
	var watcherKeys []string
	for k, _ := range data {
		watcherKeys = append(watcherKeys, k)
	}
	sort.Strings(watcherKeys)

	for _, watcher := range watcherKeys {
		var checkKeys []string = nil
		for c, _ := range data[watcher] {
			checkKeys = append(checkKeys, c)
		}
		sort.Strings(checkKeys)
		mainIndex := 0
		// find the main check and insert it
		for i, check := range checkKeys {
			if check == "main" {
				mainIndex = i
				mainEntry := &DocumentationTemplateData{
					Watcher: watcher, Check: check,
					Text:    data[watcher][check].Text,
					Version: data[watcher][check].Version,
					Header:  true,
				}
				documentation = append(documentation, mainEntry)
			}
		}
		// find chek == main
		for i, check := range checkKeys {
			doc := data[watcher][check]
			entry := &DocumentationTemplateData{
				Watcher: watcher,
				Check:   check,
				Text:    doc.Text,
				Version: doc.Version,
				Header:  false,
			}
			if len(checkKeys) == 1 {
				// only one check : it is a header
				entry.Header = true
				documentation = append(documentation, entry)
				break
			}
			if i != mainIndex {
				// not a header : insert it
				documentation = append(documentation, entry)
			} else {
				// skip the main check (already inserted)
			}
			checkKeys = nil
		}
		watcherKeys = nil
	}

	return documentation, err
}
