package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/cocoon/trainmvp/api"
	web "bitbucket.org/cocoon/web-collector/app/server"
	assets "bitbucket.org/cocoon/web-collector/assets"
)

/*


 */

var (
	natsURL         = flag.String("nats", "nats://127.0.0.1:4222", "nats url")
	collectorURL    = flag.String("server", "0.0.0.0:8080", "web collector server")
	collectorPeriod = flag.String("period", "120s", "collector period in seconds to check idle watchers")
	watchersURL     = flag.String("watchers", "http://localhost:9001", "watcher supervisor url")
	alertManagerURL = flag.String("alertmanager", "http://localhost:9093", "alert-manager url")
	grafanaURL      = flag.String("grafana", "http://localhost:3000", "graffana url")
	prometheusURL   = flag.String("prometheus", "http://localhost:9090", "prometheus url")
)

func main() {

	// subscribe to SIGINT signals
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt)

	flag.Parse()

	ctx, cancel := context.WithCancel(context.Background())
	_ = ctx

	// create a collector and start it
	c := api.NewAPI(ctx, *natsURL)
	c.Period = *collectorPeriod
	err := c.Start()
	if err != nil {
		log.Printf("%s\n", err.Error())
		return
	}
	nc, err := c.NatsConnection()
	if err != nil {
		log.Printf("%s\n", err.Error())
		return
	}

	// create web assets templater
	a := assets.NewAssets("assets", "")
	err = a.LoadTemplate()
	if err != nil {
		log.Printf("%s\n", err.Error())
		return
	}

	// create server
	s := web.NewCollectorServer(ctx, a, c)

	// add links
	s.Links["alertManager"] = *alertManagerURL
	s.Links["grafana"] = *grafanaURL
	s.Links["watchers"] = *watchersURL
	s.Links["prometheus"] = *prometheusURL

	// create web socket server
	ws := web.NewWebsocketServer()
	ws.StartWatcherStatusUpdater(nc)
	ws.RegisterWebSocket(s.Echo)

	log.Printf("web collector: starting ...")
	s.Start(*collectorURL)

	// wait for singint
	<-stopChan
	log.Println("web collector: shutting down ...")
	cancel()
	time.Sleep(2 * time.Second)

	log.Println("web collector: exited.")

}
