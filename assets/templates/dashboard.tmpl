{{define "PageContent"}}
<div class="container-fluid p-0">

    <div class="row mb-2 mb-xl-3">
        <div class="col-auto d-none d-sm-block">
            <h3><strong>Analytics</strong> Dashboard</h3>
        </div>

        <div class="col-auto ml-auto text-right mt-n1">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent p-0 mt-1 mb-0">
                    <li class="breadcrumb-item"><a href="#">Collector</a></li>
                    <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Analytics</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-4">Watchers</h5>
                    <h1 class="mt-1 mb-3">{{.NbWatchers}}</h1>
                    <div class="mb-1">
                        <span class="text-danger"> <i class="mdi mdi-arrow-bottom-right"></i></span>
                        <span class="text-muted"></span>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mb-4">Checks</h5>
                    <h1 class="mt-1 mb-3">{{.NbChecks}}</h1>
                    <div class="mb-1">
                        <span class="text-success"> <i class="mdi mdi-arrow-bottom-right"></i></span>
                        <span class="text-muted"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-xxl-3 d-flex order-2 order-xxl-3">
            <div class="card flex-fill w-100">
                <div class="card-header">

                    <h5 class="card-title mb-0">Watcher check status</h5>
                </div>
                <div class="card-body d-flex">
                    <div class="align-self-center w-100">
                        <div class="py-3">
                            <div class="chart chart-xs">
                                <canvas id="chartjs-dashboard-pie"></canvas>
                            </div>
                        </div>

                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>OK</td>
                                    <td class="text-right">{{.WatchersOK}}</td>
                                </tr>
                                <tr>
                                    <td>Down</td>
                                    <td class="text-right">{{.WatchersDown}}</td>
                                </tr>
                                <tr>
                                    <td>KO</td>
                                    <td class="text-right">{{.WatchersKO}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-12 col-lg-12 col-xxl-12 d-flex">
            <div class="card flex-fill">
                <div class="card-header">

                    <h5 class="card-title mb-0">watchers</h5>
                </div>
                <table class="table table-hover my-0">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th class="d-none d-md-table-cell">Timestamp</th>
                            <th>Status</th>
                            <th class="d-none d-xl-table-cell">Doc</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{range .WatcherStatus}}
                        <tr>
                            <td>{{.Watcher}}</td>
                            <td>{{.Timestamp}}</td>
                            <td><span class="badge {{.Badge}}">{{.Status}}</span></td>
                            <td>{{.Text}}</td>
                        </tr>
                        {{end}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
{{end}}


{{define "Scripts"}}
<script>
    document.addEventListener("DOMContentLoaded", function() {
        // Pie chart
        new Chart(document.getElementById("chartjs-dashboard-pie"), {
            type: "pie",
            data: {
                labels: ["OK", "Down", "KO"],
                datasets: [{
                    data: [{{.WatchersOK}},{{.WatchersDown}}, {{.WatchersKO}}],
                    backgroundColor: [
                        window.theme.success,
                        window.theme.warning,
                        window.theme.danger
                    ],
                    borderWidth: 5
                }]
            },
            options: {
                responsive: !window.MSInputMethodContext,
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                cutoutPercentage: 75
            }
        });
    });
</script>
{{end}}
