package web

import (
	"context"
	"testing"
	"time"

	"github.com/labstack/gommon/log"

	collectorApi "bitbucket.org/cocoon/trainmvp/api"
	"bitbucket.org/cocoon/web-collector/assets"
	// "github.com/stretchr/testify/assert"
)

func TestWeb(t *testing.T) {

	ctx, cancel := context.WithCancel(context.Background())
	a := assets.NewAssets("", "demo")
	err := a.LoadTemplate()
	if err != nil {
		log.Fatal(err.Error())
	}

	api := collectorApi.NewAPI(ctx, "nats://127.0.0.1:4222")

	server := NewCollectorServer(ctx, a, api)

	go server.Start(":8080")

	time.Sleep(1000 * time.Second)
	cancel()
	time.Sleep(2 * time.Second)
	//Server()

}
