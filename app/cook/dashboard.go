package cook

import "bitbucket.org/cocoon/trainmvp/api"

type DashboardTemplateData struct {
	NbWatchers int
	NbChecks   int

	WatchersOK   int
	WatchersKO   int
	WatchersDown int

	WatcherStatus []*WatcherStatusTemplateData
}

func DashboardTemplater(api api.CollectorRequester) (data *DashboardTemplateData, err error) {

	data = &DashboardTemplateData{}

	statuses, err := WatcherStatusTemplater(api, "all")
	if err != nil {
		return
	}

	for _, line := range statuses {
		// increment checks
		data.NbChecks += 1
		if line.Table != "" {
			// it s a status header
			data.NbWatchers += 1
			data.WatcherStatus = append(data.WatcherStatus, line)
			switch line.Status {
			case "OK":
				data.WatchersOK += 1
			case "KO":
				data.WatchersKO += 1

			default:
				data.WatchersDown += 1
			}
		}
	}
	return
}
