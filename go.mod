module bitbucket.org/cocoon/web-collector

go 1.16

require (
	bitbucket.org/cocoon/trainmvp v0.0.0-20210914091911-2b3dab6437f7
	github.com/labstack/echo-contrib v0.9.0
	github.com/labstack/echo/v4 v4.2.0
	github.com/labstack/gommon v0.3.0
	github.com/nats-io/jwt v1.2.2 // indirect
	github.com/nats-io/nats.go v1.10.0
	github.com/prometheus/common v0.17.0 // indirect
	github.com/prometheus/procfs v0.6.0 // indirect
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83 // indirect
	golang.org/x/net v0.0.0-20201021035429-f5854403a974
	golang.org/x/sys v0.0.0-20210220050731-9a76102bfb43 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
