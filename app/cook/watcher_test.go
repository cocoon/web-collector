package cook

import (
	"os"
	"testing"

	"bitbucket.org/cocoon/web-collector/app/mock"
	"bitbucket.org/cocoon/web-collector/assets"
)

func TestWatcherStatusTemplater(t *testing.T) {

	api, _ := mock.NewCollectorRequesterApi()

	data, err := WatcherStatusTemplater(api, "all")
	if err != nil {
		t.Fail()
		return
	}
	_ = data

	a := assets.NewAssets("", "")
	a.LoadTemplate()

	a.RenderScreen(os.Stdout, "watcher_check.tmpl", data)

}

func TestWatcherStatusAlertTemplater(t *testing.T) {

	api, _ := mock.NewCollectorRequesterApi()

	data, err := WatcherStatusTemplater(api, "alert")
	if err != nil {
		t.Fail()
		return
	}
	_ = data

	a := assets.NewAssets("", "")
	a.LoadTemplate()

	a.RenderScreen(os.Stdout, "watcher_check.tmpl", data)

}
